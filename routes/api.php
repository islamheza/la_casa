<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Booth\BoothController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\Events\EventController;
use App\Http\Controllers\Events\EventsSectionController;
use App\Http\Controllers\Events\EventTicketsController;
use App\Http\Controllers\Events\EventTicketcodesController;
use App\Http\Controllers\Speakers\SpeakerController;
use App\Http\Controllers\SponsorController;
use App\Http\Controllers\Team\TeamController;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//})
//
Route::get('/event/metadata', [EventController::class, 'getMetadata']);
Route::post('/event/order/create', [EventController::class, 'createPaymentOrder']);
Route::post('/event/order/callback/direct', [EventController::class, 'directCallBack']);
Route::get('/event/order/callback/redirect', [EventController::class, 'redirectCallBack']);
Route::get('/event/order/podetails', [EventController::class, 'getPaymentOrder']);


Route::group([
	'middleware' => 'api',
	'prefix' => 'auth'
		], function ($router) {
	Route::post('/login', [AuthController::class, 'login']);
	Route::post('/register', [AuthController::class, 'register']);
	Route::post('/logout', [AuthController::class, 'logout']);
	Route::post('/refresh', [AuthController::class, 'refresh']);
	Route::get('/user-profile', [AuthController::class, 'userProfile']);
});

/* Company */
Route::group([
	'prefix' => 'company',
	'middleware' => ['api', 'auth:api', 'company-injector'],
		], function ($router) {

	Route::post('/create', [CompanyController::class, 'create']);
	Route::post('/update', [CompanyController::class, 'update']);
});
/* Event */
Route::group([
	'prefix' => 'event',
	'middleware' => ['api', 'auth:api', 'company-injector'],
		], function ($router) {


	Route::post('/create', [EventController::class, 'create']);
	Route::post('/update', [EventController::class, 'update']);
	Route::get('/details', [EventController::class, 'details']);
	Route::get('/list', [EventController::class, 'list']);
});
/* Event Sections */
Route::group([
	'prefix' => 'event/section',
	'middleware' => ['api', 'auth:api', 'company-injector'],
		], function ($router) {


	Route::get('/list', [EventsSectionController::class, 'list']);
	Route::post('/create', [EventsSectionController::class, 'create']);
	Route::post('/update', [EventsSectionController::class, 'update']);
});
/* Event Ticket Types */
Route::group([
	'prefix' => 'event/tickets',
	'middleware' => ['api', 'auth:api', 'company-injector'],
		], function ($router) {


	Route::get('/list', [EventTicketsController::class, 'list']);
	Route::post('/create', [EventTicketsController::class, 'create']);
	Route::post('/update', [EventTicketsController::class, 'update']);
});
/* Event Ticket Codes */
Route::group([
	'prefix' => 'event/codes',
	'middleware' => ['api', 'auth:api', 'company-injector'],
		], function ($router) {


	Route::get('/list', [EventTicketcodesController::class, 'list']);
	Route::post('/create', [EventTicketcodesController::class, 'create']);
	Route::post('/update', [EventTicketcodesController::class, 'update']);
});

/* Event speakers  */
Route::group([
	'prefix' => 'event/speakers',
	'middleware' => ['api', 'auth:api', 'company-injector'],
		], function ($router) {

	Route::get('/list/{id}', [SpeakerController::class, 'list']);
	Route::post('/create', [SpeakerController::class, 'create']);
	Route::post('/update', [SpeakerController::class, 'update']);
});

/* Event sposores  */
Route::group([
	'prefix' => 'event/sposores',
	'middleware' => ['api', 'auth:api', 'company-injector'],
		], function ($router) {

	Route::get('/list/{id}', [SponsorController::class, 'list']);
	Route::post('/create', [SponsorController::class, 'create']);
	Route::post('/update', [SponsorController::class, 'update']);
});

/* Event booths  */
Route::group([
	'prefix' => 'event/booths',
	'middleware' => ['api', 'auth:api', 'company-injector'],
		], function ($router) {

	Route::get('/list/{id}', [BoothController::class, 'list']);
	Route::get('/details/{id}', [BoothController::class, 'view']);
	Route::post('/create', [BoothController::class, 'create']);
	Route::post('/update', [BoothController::class, 'update']);
	Route::get('/team/{id}', [BoothController::class, 'teamMembers']);

});

/* Event teams for booths  */
Route::group([
	'prefix' => 'event/booths/teams',
	'middleware' => ['api', 'auth:api', 'company-injector'],
		], function ($router) {

	Route::get('/list/{id}', [TeamController::class, 'list']);
	Route::post('/create', [TeamController::class, 'create']);
	Route::post('/update', [TeamController::class, 'update']);
});
