<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class boothSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            DB::table('booths')->insert([
                'name' => Str::random(10),
                'about' => Str::random(30),
                'address' => Str::random(8),
                'logo' => 'uploads/events/booths/202204032258499.png',
                'image' => 'uploads/events/booths/2022040322571910.png',
                'video' => 'www.youtube.com',
                'website' => 'www.vodafone.com',
                'facebook' => 'www.facebook.com',
                'instagram' => 'www.instagram.com',
                'youtube' => 'www.youtube.com',
                'event_id' => '1'
            ]);
        }

    }
}
