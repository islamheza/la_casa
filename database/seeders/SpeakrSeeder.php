<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SpeakrSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            DB::table('speakers')->insert([
                'name' => Str::random(15),
                'title' => Str::random(10),
                'avatar' => 'uploads/events/speakers/202204032140518.png',
                'event_id' => '1'
            ]);
        }
    }
}
