<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQrcodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('qrcodes', function (Blueprint $table) {
			$table->id();
			$table->string("code");
			$table->boolean("assigned")->nullable();
			$table->boolean("used")->nullable();
			$table->dateTime("checkin_at")->nullable();
			$table->unsignedBigInteger("user_id");
			$table->unsignedBigInteger("event_id");
			$table->timestamps();

			$table->foreign('event_id')->references('id')->on('events');
			$table->foreign('user_id')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('qrcodes');
	}

}
