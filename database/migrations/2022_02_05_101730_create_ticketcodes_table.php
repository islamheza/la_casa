<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketcodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('ticketcodes', function (Blueprint $table) {
			$table->id();
			$table->string("name");
			$table->integer("type")->unsigned();
			$table->text("note")->nullable();
			$table->integer("numberofuses")->nullable();
			$table->integer("actualused")->default(0);
			$table->dateTime("startduration")->nullable();
			$table->dateTime("endduration")->nullable();
			$table->integer("promotype")->unsigned()->nullable();
			$table->float("promovalue")->nullable();
			$table->unsignedBigInteger('event_id');
			$table->timestamps();
			$table->foreign('event_id')->references('id')->on('events');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('ticketcodes');
	}

}
