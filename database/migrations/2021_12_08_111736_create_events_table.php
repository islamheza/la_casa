<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('events', function (Blueprint $table) {
			$table->id();
			$table->string("name");
			$table->string("alias");
			$table->text("description")->nullable();
			$table->string("location")->nullable();
			$table->string("gpslocation")->nullable();
			$table->string("logo")->nullable();
			$table->string("banner")->nullable();
			$table->dateTime("startdate");
			$table->dateTime("enddate")->nullable();
			$table->boolean("published")->default(false);

			$table->unsignedBigInteger('company_id');
			$table->timestamps();

			$table->foreign('company_id')->references('id')->on('companies');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('events');
	}

}
