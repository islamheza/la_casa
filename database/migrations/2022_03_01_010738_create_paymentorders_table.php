<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentordersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('paymentorders', function (Blueprint $table) {
			$table->id();
			$table->string("name");
			$table->string("lname");
			$table->string("mobileno");
			$table->string("email");
			$table->string("reason");
			$table->string("address");
			$table->string("area");
			$table->string("gender");
			$table->string("agegroup");
			$table->string("merchantorderid");
			$table->string("orderid");
			$table->text("paymenturl");
			$table->string("transaction_id");
			$table->float("amount");
			$table->unsignedBigInteger('event_id');
			$table->timestamps();
			$table->foreign('event_id')->references('id')->on('events');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('paymentorders');
	}

}
