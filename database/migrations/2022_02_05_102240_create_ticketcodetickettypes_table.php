<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketcodetickettypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('ticketcodetickettypes', function (Blueprint $table) {
			$table->id();
			$table->unsignedBigInteger('ticketcode_id');
			$table->unsignedBigInteger('tickettype_id');
			$table->timestamps();

			$table->foreign('ticketcode_id')->references('id')->on('ticketcodes');
			$table->foreign('tickettype_id')->references('id')->on('tickettypes');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('ticketcodetickettypes');
	}

}
