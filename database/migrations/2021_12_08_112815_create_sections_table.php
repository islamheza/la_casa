<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('sections', function (Blueprint $table) {
			$table->id();
			$table->string("name");
			$table->integer("type");
			$table->string("title")->nullable();
			$table->text("description")->nullable();
			$table->text("extra")->nullable();
			$table->unsignedBigInteger('event_id');
			$table->float("sectionorder");
			$table->timestamps();

			$table->foreign('event_id')->references('id')->on('events');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('sections');
	}

}
