<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTickettypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('tickettypes', function (Blueprint $table) {
			$table->id();
			$table->string("name");
			$table->string("description")->nullable();
			$table->integer("type");
			$table->integer("servicefees_type")->nullable();
			$table->integer("price")->nullable();
			$table->integer("quantity")->nullable();
			$table->boolean("needapproval")->nullable();
			$table->boolean("isprivate")->nullable();
			$table->integer("min")->default(1);
			$table->integer("max")->nullable();
			$table->dateTime("startduration")->nullable();
			$table->dateTime("endduration")->nullable();
			$table->integer("paymentdeadlinetype");
			$table->integer("paymentdeadline_days")->nullable();
			$table->integer("infocollect_type")->default(1);
			$table->unsignedBigInteger('event_id');
			$table->unsignedBigInteger('fallback_ticket_id')->nullable();
			$table->timestamps();

			$table->foreign('event_id')->references('id')->on('events');
			$table->foreign('fallback_ticket_id')->references('id')->on('tickettypes');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('tickettypes');
	}

}
