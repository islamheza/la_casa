<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('tickets', function (Blueprint $table) {
			$table->id();
			$table->string("name");
			$table->string("mobileno");
			$table->string("email")->nullable();
			$table->unsignedBigInteger("event_id");
			$table->unsignedBigInteger("paymentorder_id");
			$table->timestamps();

			$table->foreign('event_id')->references('id')->on('events');
			$table->foreign('paymentorder_id')->references('id')->on('paymentorders');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('tickets');
	}

}
