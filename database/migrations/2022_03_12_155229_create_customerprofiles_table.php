<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerprofilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('customerprofiles', function (Blueprint $table) {
			$table->id();
			$table->string("name");
			$table->string("email");
			$table->string("mobileno");
			$table->string("password");
			$table->string("forgetpassword")->nullable();
			$table->unsignedBigInteger('company_id');
			$table->timestamps();

			$table->foreign('company_id')->references('id')->on('companies');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('customerprofiles');
	}

}
