<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Newsletter;

class TestMailchimp extends Command {

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'yalla:testmailchimp';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return int
	 */
	public function handle() {
		Newsletter::subscribe('ahmedadel30@hotmail.com',
				[
					'FNAME' => 'Ahmed',
					'LNAME' => 'Makki',
					'NAME' => 'Ahmed Adel',
					'MMERGE7' => '30-40',
					'MMERGE8' => 'Male',
					'MMERGE9' => "I'm getting married and furnishing my home",
					'ADDRESS' => '',
					'PHONE' => '',
				]
		);
		return Command::SUCCESS;
	}

}
