<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Platform\Events\TicketPurchaser;
use App\Models\Paymentorder;

class MarkPOasPaid extends Command {

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'yalla:markpopaid';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return int
	 */
	public function handle() {
		$paymentOrder = Paymentorder::find(15);
		$ticketPurchaser = new TicketPurchaser();
		$ticketPurchaser->markAsPaid($paymentOrder);
		return Command::SUCCESS;
	}

}
