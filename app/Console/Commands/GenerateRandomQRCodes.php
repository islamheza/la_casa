<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Qrcode;
use App\Classes\Core\Utils\TicketCodeGenerator;

class GenerateRandomQRCodes extends Command {

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'yalla:generatecodes';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';
	private $arrayOfQrCodes;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->arrayOfQrCodes = array();
	}

	/**
	 * Execute the console command.
	 *
	 * @return int
	 */
	public function handle() {
		$eventId = 2;
		$qrCodesToGenerate = 20000;
		$ticketCodeGenerator = new TicketCodeGenerator();

		for ($i = 0; $i < $qrCodesToGenerate; $i++) {
			$randomCode = $this->getNewRandomCode($ticketCodeGenerator);
			$this->arrayOfQrCodes[] = $randomCode;
		}

		$tobesaved = array();
		for ($i = 0; $i < sizeof($this->arrayOfQrCodes); $i++) {
			$tobesaved[] = array(
				"event_id" => $eventId,
				"code" => $this->arrayOfQrCodes[$i],
			);
		}
		Qrcode::insert($tobesaved);



		return Command::SUCCESS;
	}

	public function getNewRandomCode(TicketCodeGenerator $ticketCodeGenerator) {
		$randomCode = null;
		do {
			$randomCode = $ticketCodeGenerator->newPin(6);
		} while (in_array($randomCode, $this->arrayOfQrCodes));
		return $randomCode;
	}

}
