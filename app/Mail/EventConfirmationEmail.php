<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Ticket;

class EventConfirmationEmail extends Mailable {

	use Queueable,
	 SerializesModels;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public $ticket;

	public function __construct(Ticket $ticket) {
		//
		$this->ticket = $ticket;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build() {
		$address = 'lacasa@yallaevents.me';
		$subject = 'Ready to explore the world of design & furniture?';
		$name = 'La Casa Furniture Expo';
		return $this->view('emails.lacasaticket')
						->from($address, $name)
						->subject($subject);
	}

}
