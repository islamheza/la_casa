<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Sponsor;
use Illuminate\Http\Request;

class SponsorController extends Controller
{
    public function create(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string|min:3|max:190',
            'logo' => 'nullable|image',
            'event_id' => 'required|exists:events,id'
        ]);

        if ($request->validate ? $request->validate->errors() : false) {
            return response()->json($request->validate->errors(), 400);
        } else {

            if ($image = $request->file('logo')) {
                $destinationPath = 'uploads/events/sposors';
                $profileImage =  "$destinationPath/" . date('YmdHis') .  rand(6, 10) . '.' . $image->getClientOriginalExtension();
                $image->move($destinationPath, $profileImage);
                $data['logo'] = "$profileImage";
            }

            $sponsor = Sponsor::create($data);

            return response()->json($sponsor);
        }
    }

    public function update(Request $request)
    {
        $data = $request->validate([
            'id' => 'required',
            'name' => 'required|string|min:3|max:190',
            'logo' => 'nullable|image',
            'event_id' => 'required|exists:events,id'
        ]);

        if ($request->validate ? $request->validate->errors() : false) {
            return response()->json($request->validate->errors(), 400);
        } else {

            $sponsor = Sponsor::find($request->id);

            if ($image = $request->file('logo')) {
                $destinationPath = 'uploads/events/sponsors';
                $profileImage =  "$destinationPath/" . date('YmdHis') .  rand(6, 10) . '.' . $image->getClientOriginalExtension();
                $image->move($destinationPath, $profileImage);
                $data['logo'] = "$profileImage";
            }

            $sponsor = $sponsor->update($data);

            return response()->json([
                'status' => true,
                'message' => 'sponsor updated succesfully'
            ]);
        }
    }


    public function list($id)
    {
        $event = Event::find($id);
        $sponsors = $event->sponsors;

        return response()->json($sponsors);
    }

}
