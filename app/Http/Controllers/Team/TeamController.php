<?php

namespace App\Http\Controllers\Team;

use App\Http\Controllers\Controller;
use App\Models\Booth;
use App\Models\TeamMember;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    public function create(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string|min:3|max:190',
            'avatar' => 'nullable|image',
            'booth_id' => 'required|exists:booths,id'
        ]);

        if ($request->validate ? $request->validate->errors() : false) {
            return response()->json($request->validate->errors(), 400);
        } else {

            if ($image = $request->file('avatar')) {
                $destinationPath = 'uploads/events/booths/teams';
                $profileImage =  "$destinationPath/" . date('YmdHis') .  rand(6, 10) . '.' . $image->getClientOriginalExtension();
                $image->move($destinationPath, $profileImage);
                $data['avatar'] = "$profileImage";
            }

            $member = TeamMember::create($data);

            return response()->json($member);
        }
    }

    public function update(Request $request)
    {
        $data = $request->validate([
            'id' => 'required',
            'name' => 'required|string|min:3|max:190',
            'avatar' => 'nullable|image',
            'booth_id' => 'required|exists:booths,id'
        ]);

        if ($request->validate ? $request->validate->errors() : false) {
            return response()->json($request->validate->errors(), 400);
        } else {

            $speaker = TeamMember::find($request->id);

            if ($image = $request->file('avatar')) {
                $destinationPath = 'uploads/events/booths/teams';
                $profileImage =  "$destinationPath/" . date('YmdHis') .  rand(6, 10) . '.' . $image->getClientOriginalExtension();
                $image->move($destinationPath, $profileImage);
                $data['avatar'] = "$profileImage";
            }

            $speaker = $speaker->update($data);

            return response()->json([
                'status' => true,
                'message' => 'Member updated succesfully'
            ]);
        }
    }


    public function list($id)
    {
        $event = Booth::find($id);
        $members = $event->teamMembers;

        return response()->json($members);
    }

}
