<?php

namespace App\Http\Controllers\Booth;

use App\Http\Controllers\Controller;
use App\Models\Booth;
use App\Models\Event;
use Illuminate\Http\Request;

class BoothController extends Controller
{
    public function create(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string|min:3|max:190',
            'about' => 'nullable|string|max:190',
            'address' => 'nullable|string|max:190',
            'logo' => 'required|image',
            'image' => 'nullable|image',
            'video' => 'nullable|string|max:190',
            'website' => 'nullable|string|max:190',
            'facebook' => 'nullable|string|max:190',
            'instagram' => 'nullable|string|max:190',
            'youtube' => 'nullable|string|max:190',
            'event_id' => 'required|exists:events,id'
        ]);

        if ($request->validate ? $request->validate->errors() : false) {
            return response()->json($request->validate->errors(), 400);
        } else {

            if ($image = $request->file('logo')) {
                $destinationPath = 'uploads/events/booths';
                $profileImage =  "$destinationPath/" . date('YmdHis') .  rand(6, 10) . '.' . $image->getClientOriginalExtension();
                $image->move($destinationPath, $profileImage);
                $data['logo'] = "$profileImage";
            }

            if ($image = $request->file('image')) {
                $destinationPath = 'uploads/events/booths';
                $profileImage =  "$destinationPath/" . date('YmdHis') .  rand(6, 10) . '.' . $image->getClientOriginalExtension();
                $image->move($destinationPath, $profileImage);
                $data['image'] = "$profileImage";
            }

            $booth = Booth::create($data);

            return response()->json($booth);
        }
    }

    public function update(Request $request)
    {
        $data = $request->validate([
            'id' => 'required',
            'name' => 'required|string|min:3|max:190',
            'about' => 'nullable|string|max:190',
            'address' => 'nullable|string|max:190',
            'logo' => 'required|image',
            'image' => 'nullable|image',
            'video' => 'nullable|string|max:190',
            'website' => 'nullable|string|max:190',
            'facebook' => 'nullable|string|max:190',
            'instagram' => 'nullable|string|max:190',
            'youtube' => 'nullable|string|max:190',
            'event_id' => 'required|exists:events,id'
        ]);

        if ($request->validate ? $request->validate->errors() : false) {
            return response()->json($request->validate->errors(), 400);
        } else {

            $booth = Booth::find($request->id);

            if ($image = $request->file('logo')) {
                $destinationPath = 'uploads/events/booths';
                $profileImage =  "$destinationPath/" . date('YmdHis') .  rand(6, 10) . '.' . $image->getClientOriginalExtension();
                $image->move($destinationPath, $profileImage);
                $data['logo'] = "$profileImage";
            }

            if ($image = $request->file('image')) {
                $destinationPath = 'uploads/events/booths';
                $profileImage =  "$destinationPath/" . date('YmdHis') .  rand(6, 10) . '.' . $image->getClientOriginalExtension();
                $image->move($destinationPath, $profileImage);
                $data['image'] = "$profileImage";
            }

            $booth = $booth->update($data);

            return response()->json([
                'status' => true,
                'message' => 'booth updated succesfully'
            ]);
        }
    }

    public function view($id)
    {
        $booth = Booth::find($id);
        return response()->json($booth);
    }

    public function list($id)
    {
        $event = Event::find($id);
        $booths = $event->booths;

        return response()->json($booths);
    }

    public function teamMembers($id)
    {
        $booth = Booth::find($id) ;
        $members = $booth->teamMembers ;

        return response()->json($members);

    }
}
