<?php

namespace App\Http\Controllers\Events;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Classes\Platform\Events\TicketTypes\EventTicketTypeViewer;
use App\Classes\Platform\Events\TicketTypes\Populators\EventTicketTypePopulatorFactory;
use App\Classes\Platform\Events\TicketTypes\EventTicketTypeCreator;
use App\Classes\Platform\Events\TicketTypes\Populators\EventIdEventTicketTypePopulator;
use App\Classes\Platform\Events\TicketTypes\Populators\EventTypeEventTicketTypePopulator;
use App\Classes\Platform\Events\TicketTypes\EventTicketTypeUpdater;

class EventTicketsController extends Controller {

	//
	public function list(Request $request) {
		$viewer = new EventTicketTypeViewer();
		list ($list, $total) = $viewer->get($request);
		return response()->json(compact("list", "total"));
	}

	public function create(Request $request) {
		$creator = new EventTicketTypeCreator();
		EventTicketTypePopulatorFactory::get($creator, $request->input("type"));
		$creator->addPopulator(new EventIdEventTicketTypePopulator);
		$creator->addPopulator(new EventTypeEventTicketTypePopulator);
		list ($status, $record ) = $creator->process($request);
		return response()->json(compact("record", "status"));
	}

	public function update(Request $request) {
		$creator = new EventTicketTypeUpdater();
		EventTicketTypePopulatorFactory::get($creator, $request->input("type"));
		list ($status, $record ) = $creator->process($request);
		return response()->json(compact("record", "status"));
	}

}
