<?php

namespace App\Http\Controllers\Events;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Classes\Platform\Events\EventCreator;
use App\Classes\Platform\Events\EventUpdater;
use App\Classes\Platform\Events\EventViewer;
use App\Classes\Platform\Events\Populators\BaseEventPopulator;
use App\Classes\Platform\Events\Populators\AliasEventPopulator;
use App\Classes\Platform\Events\Populators\CompanyEventPopulator;
use App\Classes\Platform\Events\Populators\AdvancedEventPopulator;
use Auth;
use App\Classes\Core\Utils\SlugCreator;
use App\Classes\Core\Utils\FileUtils;
use App\Models\Event;
use App\Models\Paymentorder;
use App\Models\Ticket;
use App\Models\Tickettype;
use Illuminate\Support\Str;
use App\Classes\Platform\Payments\Drivers\PaymobDriver;
use App\Classes\Core\HTTPRequests\HTTPParams;
use App\Classes\Core\HTTPRequests\HTTPSender;
use App\Classes\Core\HTTPRequests\HTTPError;
use App\Classes\Platform\Events\TicketPurchaser;
use Newsletter;

class EventController extends Controller {

	//
	public function create(Request $request) {
		$user = Auth::user();
		$creator = new EventCreator();
		$creator->addPopulator(new BaseEventPopulator);
		$creator->addPopulator(new AliasEventPopulator(new SlugCreator));
		$creator->addPopulator(new CompanyEventPopulator($user->viewAs));
		$record = $creator->process($request);
		return response()->json(compact("record"));
	}

	public function update(Request $request) {
		$updater = new EventUpdater();
		$updater->addPopulator(new BaseEventPopulator);
		$updater->addPopulator(new AdvancedEventPopulator(new FileUtils));
		$record = $updater->process($request);
		return response()->json(compact("record"));
	}

	public function list(Request $request) {
		$user = Auth::user();
		$viewer = new EventViewer($user->viewAs);
		list ($list, $total) = $viewer->get($request);
		return response()->json(compact("list", "total"));
	}

	public function details(Request $request) {
		$record = Event::find($request->input("id"));
		$record->sections;
		$record->tickettypes;
		return response()->json(compact("record"));
	}

	public function getMetadata(Request $request) {
		$record = Event::where("alias", $request->input("alias"))->first();
		if ($record == null) {
			
		}
		$record->sections;
		$record->tickettypes;
		return response()->json(compact("record"));
	}

	public function getPaymentOrder(Request $request) {
		$paymentOrder = Paymentorder::where("id", $request->input("id"))->with(["tickets", "tickets.type"])->first();
		return response()->json(compact("paymentOrder"));
	}

	public function createPaymentOrder(Request $request) {
		$paymentorder = new Paymentorder;
		$paymentorder->name = $request->input("name");
		$paymentorder->lname = $request->input("lname");
		$paymentorder->mobileno = $request->input("mobileno");
		$paymentorder->email = $request->input("email");
		$paymentorder->reason = $request->input("reason");
		$paymentorder->address = $request->input("address");
		$paymentorder->area = $request->input("area");
		$paymentorder->gender = $request->input("gender");
		$paymentorder->agegroup = $request->input("agegroup");
		$paymentorder->event_id = $request->input("event_id");
		$ticketType = Tickettype::find($request->input("ticketid"));
		$paymentorder->amount = $this->calculateTotalAmount($request, $ticketType);
		$paymentorder->merchantorderid = "ye-" . "-" . $paymentorder->event_id . "-" . Str::random(10);
		$paymentorder->save();

		$hostTicket = new Ticket;
		$hostTicket->name = $paymentorder->name . " " . $paymentorder->lname;
		$hostTicket->mobileno = $paymentorder->mobileno;
		$hostTicket->email = $paymentorder->email;
		$hostTicket->event_id = $paymentorder->event_id;
		$hostTicket->paymentorder_id = $paymentorder->id;
		$hostTicket->tickettype_id = $ticketType->id;
		$hostTicket->price = $ticketType->price;
		$hostTicket->save();

		foreach ($request->input("guests") as $guest):
			$hostTicket = new Ticket;
			$hostTicket->name = $guest['name'];
			$hostTicket->mobileno = $guest['mobileno'];
			$hostTicket->email = $guest['email'];
			$hostTicket->event_id = $paymentorder->event_id;
			$hostTicket->paymentorder_id = $paymentorder->id;
			$hostTicket->tickettype_id = $ticketType->id;
			$hostTicket->price = $ticketType->price;
			$hostTicket->save();

		endforeach;

		$paymobDriver = new PaymobDriver(new HTTPSender(), new HTTPParams(), new HTTPError());
		$paymentLink = $paymobDriver->createPaymentLink($paymentorder);
		$paymentorder->orderid = $paymobDriver->getOrderId();
		$paymentorder->paymenturl = $paymentLink;
		$paymentorder->save();

		//Subscribe To newsletter
		$subscribeToNewsLetterFlag = $request->input("subscribe", true);
		if ($subscribeToNewsLetterFlag == true || $subscribeToNewsLetterFlag == "true" || $subscribeToNewsLetterFlag == "1" || $subscribeToNewsLetterFlag == 1) {
			Newsletter::subscribe($paymentorder->email,
					[
						'FNAME' => $paymentorder->name,
						'LNAME' => $paymentorder->lname,
						'NAME' => $paymentorder->name . " " . $paymentorder->lname,
						'MMERGE7' => $this->mapAgeBracket($paymentorder),
						'MMERGE8' => $paymentorder->gender,
						'MMERGE9' => $this->mapReason($paymentorder),
						'PHONE' => $paymentorder->mobileno,
					]
			);
		}


		return response()->json(compact("paymentorder", "paymentLink"));
	}

	private function mapAgeBracket($paymentOrder) {
		$text = "-";
		switch ($paymentOrder->agegroup) {
			case "1":case 1:
				$text = "18 - 25";
				break;
			case "2":case 2:
				$text = "26 - 35";
				break;
			case "3":case 3:
				$text = "36 - 45";
				break;
			case "4":case 4:
				$text = "45+";
				break;
		}
		return $text;
	}

	private function mapReason($paymentOrder) {
		$text = "-";
		switch ($paymentOrder->reason) {
			case "1":case 1:
				$text = "I'm getting married and furnishing my home";
				break;
			case "2":case 2:
				$text = "I'm furnishing my summer home";
				break;
			case "3":case 3:
				$text = "I'm moving to a new home";
				break;
			case "4":case 4:
				$text = "I'm furnishing multiple homes to rent or sell";
				break;
			case "5":case 5:
				$text = "I work in the design & furniture industry and want to stay up to date with the latest trends";
				break;
			case "6":case 6:
				$text = "Others";
				break;
		}
		return $text;
	}

	public function directCallBack(Request $request) {
		$paymobDriver = new PaymobDriver(new HTTPSender(), new HTTPParams(), new HTTPError());
		$response = $paymobDriver->directProcessCallback($request);
		$paymentOrder = Paymentorder::where("merchantorderid", $response['merchantOrderId'])->first();
		$paymentOrder->transaction_id = $response['transactionId'];
		$paymentOrder->orderid = $response['orderId'];
		$paymentOrder->paid = $response['isSuccess'] == "true";
		$paymentOrder->message = $response['message'];
		$paymentOrder->processed = 1;
		$paymentOrder->save();


		if ($paymentOrder->paid) {
			$ticketPurchaser = new TicketPurchaser();
			$ticketPurchaser->markAsPaid($paymentOrder);
		}

		return response()->json(compact("paymentOrder"));
	}

	public function redirectCallBack(Request $request) {
		$paymobDriver = new PaymobDriver(new HTTPSender(), new HTTPParams(), new HTTPError());
		$response = $paymobDriver->redirectCallback($request);
		sleep(1);
		$paymentOrder = Paymentorder::where("merchantorderid", $response['merchantOrderId'])->first();
		$event = Event::find($paymentOrder->event_id);
		$link = config("app.url") . "register-event/iframe/" . $event->alias . "/withcallback/" . $paymentOrder->id;
		//return response()->json(compact("link"));
		return redirect($link);
	}

	private function calculateTotalAmount(Request $request, $ticketType) {
		$quantity = (int) $request->input("quantity");
		$finalAmount = $ticketType->price * $quantity;
		return ceil($finalAmount);
	}

}
