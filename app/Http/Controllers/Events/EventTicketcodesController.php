<?php

namespace App\Http\Controllers\Events;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Classes\Platform\Events\TicketCodes\TicketcodeViewer;
use App\Classes\Platform\Events\TicketCodes\TicketcodeCreator;
use App\Classes\Platform\Events\TicketCodes\TicketcodeUpdater;
use App\Classes\Platform\Events\TicketCodes\Populators\TicketcodePopulatorFactory;
use App\Classes\Platform\Events\TicketCodes\Populators\EventIdTicketcodePopulator;

class EventTicketcodesController extends Controller {

	//
	public function list(Request $request) {
		$viewer = new TicketcodeViewer();
		list ($list, $total) = $viewer->get($request);
		return response()->json(compact("list", "total"));
	}

	public function create(Request $request) {
		$creator = new TicketcodeCreator();
		TicketcodePopulatorFactory::get($creator, $request->input("type"));
		$creator->addPopulator(new EventIdTicketcodePopulator);
		list ($status, $record ) = $creator->process($request);
		return response()->json(compact("record", "status"));
	}

	public function update(Request $request) {
		$creator = new TicketcodeUpdater();
		TicketcodePopulatorFactory::get($creator, $request->input("type"));
		list ($status, $record ) = $creator->process($request);
		return response()->json(compact("record", "status"));
	}

}
