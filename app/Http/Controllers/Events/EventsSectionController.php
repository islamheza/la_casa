<?php

namespace App\Http\Controllers\Events;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Classes\Platform\Events\PageBuilder\EventSectionCreator;
use App\Classes\Platform\Events\PageBuilder\EventSectionUpdater;
use App\Classes\Platform\Events\PageBuilder\EventSectionViewer;
use App\Classes\Platform\Events\PageBuilder\Populators\EventSectionPopulatorFactory;
use App\Classes\Platform\Events\PageBuilder\Populators\EventIdEventSectionPopulator;
use App\Classes\Core\CURD\BaseCreator;
use Auth;

class EventsSectionController extends Controller {

	//
	public function list(Request $request) {
		$viewer = new EventSectionViewer();
		list ($list, $total) = $viewer->get($request);
		return response()->json(compact("list", "total"));
	}

	public function create(Request $request) {
		$creator = new EventSectionCreator();
		EventSectionPopulatorFactory::get($creator, $request->input("type"));
		$creator->addPopulator(new EventIdEventSectionPopulator);
		list ($status, $record ) = $creator->process($request);
		return response()->json(compact("record", "status"));
	}

	public function update(Request $request) {
		$creator = new EventSectionUpdater();
		EventSectionPopulatorFactory::get($creator, $request->input("type"));
		$record = $creator->process($request);
		return response()->json(compact("record"));
	}

}
