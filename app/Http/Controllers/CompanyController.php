<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\Platform\Companies\CompanyCreator;
use App\Classes\Platform\Companies\CompanyUpdater;
use App\Classes\Platform\Companies\Populators\BaseCompanyPopulator;
use Auth;

class CompanyController extends Controller {

	//
	public function create(Request $request) {
		$creator = new CompanyCreator();
		$creator->addPopulator(new BaseCompanyPopulator);
		$record = $creator->process($request);
		return response()->json(compact("record"));
	}

	public function update(Request $request) {
		$updater = new CompanyUpdater();
		$updater->addPopulator(new BaseCompanyPopulator);
		$record = $updater->process($request);
		return response()->json(compact("record"));
	}

}
