<?php

namespace App\Http\Controllers\Speakers;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Speaker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class SpeakerController extends Controller
{
    public function create(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string|min:3|max:190',
            'title' => 'required|string|max:190',
            'avatar' => 'nullable|image',
            'event_id' => 'required|exists:events,id'
        ]);

        if ($request->validate ? $request->validate->errors() : false) {
            return response()->json($request->validate->errors(), 400);
        } else {

            if ($image = $request->file('avatar')) {
                $destinationPath = 'uploads/events/speakers';
                $profileImage =  "$destinationPath/" . date('YmdHis') .  rand(6, 10) . '.' . $image->getClientOriginalExtension();
                $image->move($destinationPath, $profileImage);
                $data['avatar'] = "$profileImage";
            }

            $speaker = Speaker::create($data);

            return response()->json($speaker);
        }
    }

    public function update(Request $request)
    {
        $data = $request->validate([
            'id' => 'required',
            'name' => 'required|string|min:3|max:190',
            'title' => 'required|string|max:190',
            'avatar' => 'nullable|image',
            'event_id' => 'required|exists:events,id'
        ]);

        if ($request->validate ? $request->validate->errors() : false) {
            return response()->json($request->validate->errors(), 400);
        } else {

            $speaker = Speaker::find($request->id);

            if ($image = $request->file('avatar')) {
                $destinationPath = 'uploads/events/speakers';
                $profileImage =  "$destinationPath/" . date('YmdHis') .  rand(6, 10) . '.' . $image->getClientOriginalExtension();
                $image->move($destinationPath, $profileImage);
                $data['avatar'] = "$profileImage";
            }

            $speaker = $speaker->update($data);

            return response()->json([
                'status' => true,
                'message' => 'speaker updated succesfully'
            ]);
        }
    }


    public function list($id)
    {
        $event = Event::find($id);
        $speakers = $event->speakers;

        return response()->json($speakers);
    }
}
