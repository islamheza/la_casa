<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CompanyInjector {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
	 * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
	 */
	public function handle(Request $request, Closure $next) {
		app('auth')->user()->viewAs = 1;
		app('auth')->user()->isSuperAdmin = true;
		return $next($request);
	}

}
