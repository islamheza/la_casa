<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Companies\Populators;

/**
 * Description of BaseCompanyPopulator
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;
use App\Classes\Core\CURD\Contracts\Populator;
use Illuminate\Support\Facades\Validator;

class BaseCompanyPopulator implements Populator {

	//put your code here
	public function set($record, Request $request) {
		$record->name = $request->input("name");
	}

	public function validate(Request $request) {
		$validator = Validator::make($request->all(), [
					'name' => 'required',
		]);
		return $validator;
	}

}
