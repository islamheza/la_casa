<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Companies;

/**
 * Description of CompanyCreator
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;
use App\Models\Company;
use App\Classes\Core\CURD\BaseUpdater;

class CompanyUpdater extends BaseUpdater {

	public function initRecord(Request $request) {
		$this->record = Company::find($request->input("id"));
	}

	public function afterSave($params) {
		
	}

	public function beforeSaveCheck($params) {
		return true;
	}

}
