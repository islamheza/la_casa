<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\CustomerProfiles;

/**
 * Description of CustomerprofileCreator
 *
 * @author AhmedMakki
 */
use App\Models\Customerprofile;

class CustomerprofileCreator {

	//put your code here
	private $companyId;

	function __construct($companyId) {
		$this->companyId = $companyId;
	}

	public function create($params) {
		$customerprofile = new Customerprofile;
		$customerprofile->name = $params['name'];
		$customerprofile->email = $params['email'];
		$customerprofile->mobileno = $params['mobileno'];
		$customerprofile->password = bcrypt($params['password']);
		$customerprofile->company_id = $this->companyId;
		$customerprofile->save();
		return $customerprofile;
	}

}
