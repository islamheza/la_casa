<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Payments;

/**
 * Description of PaymentLinkCreator
 *
 * @author AhmedMakki
 */
use App\Models\Paymentorder;
use App\Classes\Platform\Payments\Contracts\PaymentDriver;

class PaymentLinkCreator {

	private $paymentDriver;

	function __construct(PaymentDriver $paymentDriver) {
		$this->paymentDriver = $paymentDriver;
	}

	//put your code here
	public function createLink(Paymentorder $paymentOrder) {
		return $this->paymentDriver->createPaymentLink($paymentOrder);
	}

}
