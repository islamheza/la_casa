<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Payments\Drivers;

/**
 * Description of PaymobDriver
 *
 * @author AhmedMakki
 */
use App\Classes\Platform\Payments\Contracts\PaymentDriver;
use App\Models\Paymentorder;
use App\Classes\Core\HTTPRequests\HTTPParams;
use App\Classes\Core\HTTPRequests\HTTPSender;
use App\Classes\Core\HTTPRequests\HTTPError;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;

class PaymobDriver implements PaymentDriver {

	protected $httpParams;
	protected $httpSender;
	protected $httpError;
	protected $token;
	protected $orderId;
	protected $paymentKey;

	const PAYMENTLINK_LIFESPAN = 1800;
	const MERCHANTID = 73802;
	const IFRAMEID = "180736";
	const API_VALUE = "ZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6VXhNaUo5LmV5SnVZVzFsSWpvaWFXNXBkR2xoYkNJc0luQnliMlpwYkdWZmNHc2lPamN6T0RBeUxDSmpiR0Z6Y3lJNklrMWxjbU5vWVc1MEluMC5nWmdtZmliU3ROTm5NM2FYdjl0Yk9HZW0wR1dfRlVYTUVyczFTU0xmYmFNQ2NDR0piSzhTOXN6ZUpubW9lQXMwc2J6eTJRaWV1anBnU2pka2tiM2Nldw==";
	const URL_AUTH_REQUEST = "https://accept.paymobsolutions.com/api/auth/tokens";
	const URL_ORDER_REGISTRATION = "https://accept.paymobsolutions.com/api/ecommerce/orders";
	const URL_ORDER_DELETION = "https://accept.paymobsolutions.com/api/ecommerce/orders";
	const URL_PAYMENTKEY_CREATION = "https://accept.paymobsolutions.com/api/acceptance/payment_keys";
	const URL_ORDER_VOID = "https://accept.paymobsolutions.com/api/acceptance/void_refund/void";
	const URL_PAYMENT_LINK = "https://accept.paymobsolutions.com/api/acceptance/iframes";
	const KEY_API = "api_key";
	const KEY_TOKEN = "token";
	const KEY_DELIVERYNEEDED = "delivery_needed";
	const KEY_MERCHANTID = "merchant_id";
	const KEY_AMOUNT = "amount_cents";
	const KEY_CURRENY = "currency";
	const KEY_MERCHANTORDERID = "merchant_order_id";
	const KEY_ITEMS = "items";
	const KEY_SHIPPINGDATA = "shipping_data";
	const KEY_ID = "id";
	const KEY_ORDERID = "order_id";
	const KEY_EXPIRATION = "expiration";
	const KEY_BILLING_DATA = "billing_data";
	const KEY_CARDINTEGRATIONID = "card_integration_id";
	const KEY_TRANSACTIONID = "transaction_id";
	const KEY_AUTHTOKEN = "auth_token";
	const KEY_PAYMENTTOKEN = "payment_token";
	const KEY_COMMISSIONS = "commission_fees";

	//put your code here
	public function __construct(HTTPSender $httpSender, HTTPParams $httpParams, HTTPError $httpError) {
		$this->httpSender = $httpSender;
		$this->httpParams = $httpParams;
		$this->httpError = $httpError;
		$this->token = null;
	}

	public function createPaymentLink(Paymentorder $po) {
		$this->authenticate();
		$this->generateOrderId($po);
		$this->generatePaymentKey($po);
		//$po->saveOrderIdAndPaymentKey($this->orderId, $this->paymentKey);
		return $this->getPaymentLink($this->paymentKey);
	}

	protected function authenticate() {
		$this->httpParams->reset();
		$this->httpParams->setUrl(self::URL_AUTH_REQUEST);
		$this->httpParams->setMethod("POST");
		$this->httpParams->setType(HTTPParams::TYPE_JSON);
		$this->httpParams->addParam(self::KEY_API, self::API_VALUE);
		$response = $this->httpSender->send($this->httpParams);
		$arrayOfResponse = json_decode($response, true);
		$this->token = $arrayOfResponse[self::KEY_TOKEN];
	}

	protected function generateOrderId(PaymentOrder $po) {
		$this->httpParams->reset();
		$this->httpParams->setUrl(self::URL_ORDER_REGISTRATION);
		$this->httpParams->setMethod("POST");
		$this->httpParams->setType(HTTPParams::TYPE_JSON);
		$this->httpParams->addGETParam(self::KEY_TOKEN, $this->token);
		$this->httpParams->addParam(self::KEY_DELIVERYNEEDED, false);
		$this->httpParams->addParam(self::KEY_MERCHANTID, self::MERCHANTID);
		$this->httpParams->addParam(self::KEY_AMOUNT, $this->getTotal($po));
		$this->httpParams->addParam(self::KEY_CURRENY, "EGP");
		$this->httpParams->addParam(self::KEY_MERCHANTORDERID, $po->merchantorderid);
		$this->httpParams->addParam(self::KEY_ITEMS, $this->getArrayOfOrders($po));
		//$this->httpParams->addParam(AcceptDriver::KEY_SHIPPINGDATA, array());
		$response = $this->httpSender->send($this->httpParams);
		$arrayOfResponse = json_decode($response, true);
		$this->orderId = $arrayOfResponse[self::KEY_ID];
	}

	protected function generatePaymentKey(PaymentOrder $po) {
		$this->httpParams->reset();
		$this->httpParams->setUrl(self::URL_PAYMENTKEY_CREATION);
		$this->httpParams->setMethod("POST");
		$this->httpParams->setType(HTTPParams::TYPE_JSON);
		$this->httpParams->addGETParam(self::KEY_TOKEN, $this->token);
		$this->httpParams->addParam(self::KEY_AMOUNT, $this->getTotal($po));
		$this->httpParams->addParam(self::KEY_EXPIRATION, self::PAYMENTLINK_LIFESPAN);
		$this->httpParams->addParam(self::KEY_ORDERID, $this->orderId);
		$this->httpParams->addParam(self::KEY_CURRENY, "EGP");
		$this->httpParams->addParam(self::KEY_BILLING_DATA, $this->getBillingData($po));
		$this->httpParams->addParam(self::KEY_CARDINTEGRATIONID, $this->getCardIntegrationId());
		$response = $this->httpSender->send($this->httpParams);
		$arrayOfResponse = json_decode($response, true);
		$this->paymentKey = $arrayOfResponse[self::KEY_TOKEN];
	}

	private function getPaymentLink($paymentKey) {
		return self::URL_PAYMENT_LINK . "/" . self::IFRAMEID . "?" . self::KEY_PAYMENTTOKEN . "=" . $paymentKey;
	}

	public function directProcessCallback(Request $request) {
		$params = $request->input("obj");
		$merchantId = $params["order"]["merchant_order_id"];
		$orderId = $params["order"]["id"];
		$transactionId = $params["id"];
		$isSuccess = $params["success"];
		$message = $params["data"]["message"];
		return array(
			"merchantOrderId" => $merchantId,
			"orderId" => $orderId,
			"transactionId" => $transactionId,
			"isSuccess" => $isSuccess,
			"message" => $message,
		);
	}

	public function redirectCallback(Request $request) {
		$params = $request->all();
		$merchantId = $params["merchant_order_id"];
		$orderId = $params["order"];
		$transactionId = $params["id"];
		$isSuccess = $params["success"];
		$message = "Not Approved";
		if (isset($params["txn_response_code"])) {
			$message = $params["txn_response_code"];
		}

		return array(
			"merchantOrderId" => $merchantId,
			"orderId" => $orderId,
			"transactionId" => $transactionId,
			"isSuccess" => $isSuccess,
			"message" => $message,
		);
	}

	private function getArrayOfOrders(PaymentOrder $po) {
		$orders = array();
		$orders[] = array(
			"name" => "Purchasing Ticket(s) from YallaEvents",
			"amount_cents" => ($po->amount * 100)
		);
		return $orders;
	}

	private function getTotal(PaymentOrder $po) {
		return ($po->amount * 100);
	}

	private function getBillingData(PaymentOrder $po) {
		return array(
			"first_name" => $po->name,
			"last_name" => $po->lname,
			"email" => $po->email,
			"phone_number" => $po->mobileno,
			"street" => "Heliopolis",
			"city" => "Cairo",
			"country" => "Egypt",
			"apartment" => "NA",
			"floor" => "NA",
			"building" => "NA",
		);
	}

	protected function getCardIntegrationId() {
		//return 192139;
		return 219157;
	}

	public function getOrderId() {
		return $this->orderId;
	}

}
