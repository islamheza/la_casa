<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Payments\Contracts;

/**
 *
 * @author AhmedMakki
 */
use App\Models\Paymentorder;

interface PaymentDriver {

	//put your code here
	public function createPaymentLink(Paymentorder $po);
}
