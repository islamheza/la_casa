<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events\Populators;

/**
 * Description of BaseEventPopulator
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;
use App\Classes\Core\CURD\Contracts\Populator;
use Illuminate\Support\Facades\Validator;

class BaseEventPopulator implements Populator {

	//put your code here
	public function set($record, Request $request) {
		$record->name = $request->input("name");
		$record->startdate = $request->input("startdate");
		$record->enddate = $request->input("enddate", null);
	}

	public function validate(Request $request) {
		$validator = Validator::make($request->all(), [
					'name' => 'required',
					'startdate' => 'required'
		]);
		return $validator;
	}

}
