<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events\Populators;

/**
 * Description of CompanyEventPopulator
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;
use App\Classes\Core\CURD\Contracts\Populator;
use Illuminate\Support\Facades\Validator;

class CompanyEventPopulator implements Populator {

	private $companyId;

	function __construct($companyId) {
		$this->companyId = $companyId;
	}

	//put your code here
	public function set($record, Request $request) {
		$record->company_id = $this->companyId;
	}

	public function validate(Request $request) {
		$validator = Validator::make($request->all(), []);
		return $validator;
	}

}
