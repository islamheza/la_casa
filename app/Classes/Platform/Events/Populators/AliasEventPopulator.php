<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events\Populators;

/**
 * Description of AliasCompanyPopulator
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;
use App\Classes\Core\CURD\Contracts\Populator;
use Illuminate\Support\Facades\Validator;
use App\Classes\Core\Utils\SlugCreator;
use App\Models\Event;

class AliasEventPopulator implements Populator {

	private $slugCreator;

	function __construct(SlugCreator $slugCreator) {
		$this->slugCreator = $slugCreator;
	}

	//put your code here
	public function set($record, Request $request) {
		$name = $request->input("name");
		$alias = $this->generateNewSlug($name);
		$numberOfRecordsWithSameAlias = Event::where("alias", $alias)->count();
		$index = 1;
		while ($numberOfRecordsWithSameAlias > 0) {
			$alias = $this->generateNewSlug($name . $index);
			$numberOfRecordsWithSameAlias = Event::where("alias", $alias)->count();
			$index++;
		}
		$record->alias = $alias;
	}

	private function generateNewSlug($name) {
		$alias = $this->slugCreator->create($name);
		return $alias;
	}

	public function validate(Request $request) {
		$validator = Validator::make($request->all(), [
					'name' => 'required',
		]);
		return $validator;
	}

}
