<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events\Populators;

/**
 * Description of AdvancedEventPopulator
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;
use App\Classes\Core\CURD\Contracts\Populator;
use Illuminate\Support\Facades\Validator;
use App\Classes\Core\Utils\FileUtils;

class AdvancedEventPopulator implements Populator {

	//put your code here
	private $fileUtils;

	function __construct(FileUtils $fileUtils) {
		$this->fileUtils = $fileUtils;
	}

	public function set($record, Request $request) {
		$record->description = $request->input("description", null);
		$record->location = $request->input("location", null);
		$record->gpslocation = $request->input("gpslocation", null);
		$this->refreshFile($record, $request, "logo");
		$this->refreshFile($record, $request, "banner");
	}

	public function validate(Request $request) {
		$validator = Validator::make($request->all(), [
		]);
		return $validator;
	}

	private function refreshFile($record, Request $request, $dbKey) {
		$fileName = $this->fileUtils->uploadFile($request, $dbKey, "/uploads/events", $dbKey . "_");
		if ($fileName) {
			if ($record[$dbKey] != "" && $record[$dbKey] != null) {
				//Delete Old Picture
				$this->fileUtils->deleteFileFromLocal("uploads/events/" . $record[$dbKey]);
			}
			$record[$dbKey] = $fileName;
		}
	}

}
