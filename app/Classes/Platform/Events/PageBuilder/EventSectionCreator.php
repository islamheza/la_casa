<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events\PageBuilder;

/**
 * Description of EventSectionCreator
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;
use App\Models\Section;
use App\Classes\Core\CURD\BaseCreator;

class EventSectionCreator extends BaseCreator {

	//put your code here
	public function afterSave(Request $request) {
		
	}

	public function beforeSaveCheck(Request $request) {
		return true;
	}

	public function initRecord(Request $request) {
		$this->record = new Section;
	}

}
