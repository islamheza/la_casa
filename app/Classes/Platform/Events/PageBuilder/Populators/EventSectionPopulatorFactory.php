<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events\PageBuilder\Populators;

/**
 * Description of EventSectionPopulatorFactory
 *
 * @author AhmedMakki
 */
use App\Classes\Core\CURD\BaseCreator;
use App\Classes\Platform\Events\EventConstants;

class EventSectionPopulatorFactory {

	//put your code here
	public static function get(BaseCreator $baseCrudObject, $type) {
		$baseCrudObject->addPopulator(new BaseEventSectionPopulator);
		switch ($type) {
			case EventConstants::EVENTSECTIONTYPE_HTML:
				$baseCrudObject->addPopulator(new HTMLTypeEventSectionPopulator);
				break;
		}
	}

}
