<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events\PageBuilder\Populators;

/**
 * Description of HTMLTypeEventSectionPopulator
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;
use App\Classes\Core\CURD\Contracts\Populator;
use Illuminate\Support\Facades\Validator;
use App\Classes\Platform\Events\EventConstants;

class HTMLTypeEventSectionPopulator implements Populator {

	//put your code here
	public function set($record, Request $request) {
		$record->type = EventConstants::EVENTSECTIONTYPE_HTML;
		$record->title = $request->input("title");
		$record->description = $request->input("description");
	}

	public function validate(Request $request) {
		$validator = Validator::make($request->all(), [
					'title' => 'required',
					'description' => 'required'
		]);
		return $validator;
	}

}
