<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events;

/**
 * Description of EventCreator
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;
use App\Models\Event;
use App\Classes\Core\CURD\BaseCreator;

class EventCreator extends BaseCreator {

	//put your code here
	public function initRecord(Request $request) {
		$this->record = new Event;
	}

	public function afterSave($params) {
		
	}

	public function beforeSaveCheck($params) {
		return true;
	}

}
