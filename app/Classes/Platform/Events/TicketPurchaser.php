<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events;

/**
 * Description of TicketPurchaser
 *
 * @author AhmedMakki
 */
use App\Models\Paymentorder;
use App\Models\Ticket;
use App\Models\Qrcode;
use App\Models\Event;
use App\Classes\Core\Utils\SendSMSApi;
use App\Classes\Core\HTTPRequests\HTTPParams;
use App\Classes\Core\HTTPRequests\HTTPSender;
use App\Classes\Core\HTTPRequests\HTTPError;
use App\Classes\Core\Utils\WebRequestSignatureValidator;
use App\Classes\Core\Utils\URLShortener;
use App\Mail\EventConfirmationEmail;
use Illuminate\Support\Facades\Mail;

class TicketPurchaser {

	//put your code here
	public function markAsPaid(Paymentorder $po) {
		$tickets = Ticket::where("paymentorder_id", $po->id)->get();
		$this->assignQrCodes($po, $tickets);
		$this->sendSMS($po, $tickets);
	}

	private function assignQrCodes(Paymentorder $po, $tickets) {

		$httpParams = new HTTPParams();
		$httpSender = new HTTPSender();
		$httpError = new HTTPError();
		$webRequestSignatureValidator = new WebRequestSignatureValidator();

		$uRLShortener = new URLShortener($webRequestSignatureValidator, $httpSender, $httpParams, $httpError);
		$event = Event::find($po->event_id);
		foreach ($tickets as $ticket):			
			$qrCode = Qrcode::where("event_id", $po->event_id)->whereNull("assigned")->first();
			$link = config("app.url") . "register-event/" . $event->alias . "/qrcode/" . $qrCode->code;
			$shortenLink = $uRLShortener->getLink($link);
			$qrCode->assigned = 1;
			$qrCode->save();
			$ticket->qrcode_id = $qrCode->id;
			$ticket->qrcode_url = $shortenLink;
			$ticket->save();
		endforeach;
	}

	public function sendSMS($po, $tickets) {
		$sendSMSApi = new SendSMSApi();
		$first = true;
		foreach ($tickets as $ticket):
			$sendSMSApi->process($ticket);
			if ($first == true) {
				Mail::to($ticket->email)->send(new EventConfirmationEmail($ticket));
			}
			$first = false;
		endforeach;
	}

}
