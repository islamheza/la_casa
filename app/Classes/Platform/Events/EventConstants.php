<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events;

/**
 * Description of EventConstants
 *
 * @author AhmedMakki
 */
class EventConstants {

	//put your code here
	const EVENTSECTIONTYPE_HTML = 1;

	/* Ticket Types */
	const EVENTTICKETTYPE_PAID = 1;
	const EVENTTICKETTYPE_FREE = 2;
	//================
	const EVENTTICKETSERVICEFEE_ONBUYER = 1;
	const EVENTTICKETSERVICEFEE_ONORGAINZER = 2;
	//================	
	const EVENTTICKETINFOCOLLECT_GUESTLIST = 1;
	const EVENTTICKETINFOCOLLECT_BUYERONLY = 2;
	//================
	const EVENTTICKETPAYMENTTYPEDEADLINE_INSTANTLY = 1;
	const EVENTTICKETPAYMENTTYPEDEADLINE_NODEADLINE = 2;
	const EVENTTICKETPAYMENTTYPEDEADLINE_WITHINDAYS = 3;
	//================
	/* Ticket Codes */
	const EVENTTICKETCODETYPE_PASSCODE = 1;
	const EVENTTICKETCODETYPE_DISCOUNTCODE = 2;

}
