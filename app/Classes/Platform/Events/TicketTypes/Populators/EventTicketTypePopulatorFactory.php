<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events\TicketTypes\Populators;

/**
 * Description of EventTicketTypePopulatorFactory
 *
 * @author AhmedMakki
 */
use App\Classes\Core\CURD\BaseCreator;
use App\Classes\Platform\Events\EventConstants;

class EventTicketTypePopulatorFactory {

	//put your code here
	public static function get(BaseCreator $baseCrudObject, $type) {
		$baseCrudObject->addPopulator(new BaseEventTicketTypePopulator);
		switch ($type) {
			case EventConstants::EVENTTICKETTYPE_PAID:
				$baseCrudObject->addPopulator(new PaidEventTickeTypePopulator);
				break;
		}
	}

}
