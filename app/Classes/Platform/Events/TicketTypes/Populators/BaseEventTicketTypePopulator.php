<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events\TicketTypes\Populators;

/**
 * Description of BaseEventTicketTypePopulator
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;
use App\Classes\Core\CURD\Contracts\Populator;
use Illuminate\Support\Facades\Validator;

class BaseEventTicketTypePopulator implements Populator {

	//put your code here
	public function set($record, Request $request) {
		$record->name = $request->input("name");
		$record->description = $request->input("description");
		$record->quantity = $request->input("quantity");
		$record->needapproval = $request->input("needapproval", 0);
		$record->isprivate = $request->input("isprivate", 0);
		$record->min = $request->input("min");
		$record->max = $request->input("max");
		$record->startduration = $request->input("startduration", null);
		$record->endduration = $request->input("endduration", null);
		$record->infocollect_type = $request->input("infocollect_type");
	}

	public function validate(Request $request) {
		$validator = Validator::make($request->all(), [
					'name' => 'required'
		]);
		return $validator;
	}

}
