<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events\TicketTypes\Populators;

/**
 * Description of PaidEventTickeTypePopulator
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;
use App\Classes\Core\CURD\Contracts\Populator;
use Illuminate\Support\Facades\Validator;

class PaidEventTickeTypePopulator implements Populator {

	//put your code here
	public function set($record, Request $request) {
		$record->servicefees_type = $request->input("servicefees_type");
		$record->price = $request->input("price");
		$record->paymentdeadlinetype = $request->input("paymentdeadlinetype");
		$record->paymentdeadline_days = $request->input("paymentdeadline_days", null);
	}

	public function validate(Request $request) {
		$validator = Validator::make($request->all(), [
					'servicefees_type' => 'required',
					'price' => 'required',
					'paymentdeadlinetype' => 'required',
		]);
		return $validator;
	}

}
