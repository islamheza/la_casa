<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events\TicketTypes;

/**
 * Description of EventTicketTypeCreator
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;
use App\Models\Tickettype;
use App\Classes\Core\CURD\BaseCreator;

class EventTicketTypeCreator extends BaseCreator {

	//put your code here
	public function afterSave(Request $request) {
		
	}

	public function beforeSaveCheck(Request $request) {
		return true;
	}

	public function initRecord(Request $request) {
		$this->record = new Tickettype;
	}

}
