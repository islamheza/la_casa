<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events\TicketTypes;

/**
 * Description of EventTicketTypeViewer
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;
use App\Models\Tickettype;
use App\Classes\Core\CURD\BaseViewer;

class EventTicketTypeViewer extends BaseViewer {

	//put your code here
	protected function createListQuery(Request $request) {
		$query = Tickettype::query();
		$query->where("event_id", $request->input("event_id"));
		return $query;
	}

}
