<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events;

/**
 * Description of EventCreator
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;
use App\Models\Event;
use App\Classes\Core\CURD\BaseUpdater;

class EventUpdater extends BaseUpdater {

	//put your code here
	public function initRecord(Request $request) {
		$this->record = Event::find($request->input("id"));
	}

	public function afterSave($params) {
		
	}

	public function beforeSaveCheck($params) {
		return true;
	}

}
