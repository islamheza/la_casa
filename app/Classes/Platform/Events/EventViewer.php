<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events;

/**
 * Description of EventViewer
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;
use App\Models\Event;
use App\Classes\Core\CURD\BaseViewer;

class EventViewer extends BaseViewer {

	private $companyId;

	function __construct($companyId) {
		$this->companyId = $companyId;
	}

	//put your code here
	protected function createListQuery(Request $request) {
		$query = Event::query();		
		$query->where("company_id", $this->companyId);
		return $query;
	}

}
