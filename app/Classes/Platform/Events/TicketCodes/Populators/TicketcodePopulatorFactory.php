<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events\TicketCodes\Populators;

/**
 * Description of TicketcodePopulatorFactory
 *
 * @author AhmedMakki
 */
use App\Classes\Core\CURD\BaseCreator;
use App\Classes\Platform\Events\EventConstants;

class TicketcodePopulatorFactory {

	//put your code here
	public static function get(BaseCreator $baseCrudObject, $type) {
		$baseCrudObject->addPopulator(new BaseTicketcodePopulator);
		switch ($type) {
			case EventConstants::EVENTTICKETCODETYPE_DISCOUNTCODE:
				$baseCrudObject->addPopulator(new DiscountTicketcodePopulator);
				break;
		}
	}

}
