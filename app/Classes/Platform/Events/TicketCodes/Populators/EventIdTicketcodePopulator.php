<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events\TicketCodes\Populators;

/**
 * Description of EventIdTicketcodePopulator
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;
use App\Classes\Core\CURD\Contracts\Populator;
use Illuminate\Support\Facades\Validator;

class EventIdTicketcodePopulator implements Populator {

	//put your code here
	public function set($record, Request $request) {
		$record->event_id = $request->input("event_id");
	}

	public function validate(Request $request) {
		$validator = Validator::make($request->all(), [
					'event_id' => 'required'
		]);
		return $validator;
	}

}
