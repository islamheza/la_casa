<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events\TicketCodes\Populators;

/**
 * Description of DiscountTicketcodePopulator
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;
use App\Classes\Core\CURD\Contracts\Populator;
use Illuminate\Support\Facades\Validator;

class DiscountTicketcodePopulator implements Populator {

	//put your code here
	public function set($record, Request $request) {
		$record->startduration = $request->input("startduration");
		$record->endduration = $request->input("endduration");
		$record->promotype = $request->input("promotype");
		$record->promovalue = $request->input("promovalue");
	}

	public function validate(Request $request) {
		$validator = Validator::make($request->all(), [
					'startduration' => 'required',
					'endduration' => 'required',
					'promotype' => 'required',
					'promovalue' => 'required'
		]);
		return $validator;
	}

}
