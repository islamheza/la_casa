<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events\TicketCodes\Populators;

/**
 * Description of BaseTicketcodePopulator
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;
use App\Classes\Core\CURD\Contracts\Populator;
use Illuminate\Support\Facades\Validator;

class BaseTicketcodePopulator implements Populator {

	//put your code here
	public function set($record, Request $request) {
		$record->name = $request->input("name");
		$record->type = $request->input("type");
		$record->numberofuses = $request->input("numberofuses", null);
		$record->note = $request->input("note", null);
	}

	public function validate(Request $request) {
		$validator = Validator::make($request->all(), [
					'name' => 'required',
					'type' => 'required'
		]);
		return $validator;
	}

}
