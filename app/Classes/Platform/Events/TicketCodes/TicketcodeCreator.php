<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events\TicketCodes;

/**
 * Description of TicketcodeCreator
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;
use App\Models\Ticketcode;
use App\Classes\Core\CURD\BaseCreator;

class TicketcodeCreator extends BaseCreator {

	//put your code here
	public function afterSave(Request $request) {
		$this->record->tickettypes()->sync([$request->input("tickettypes")]);
	}

	public function beforeSaveCheck(Request $request) {
		return true;
	}

	public function initRecord(Request $request) {
		$this->record = new Ticketcode;
	}

}
