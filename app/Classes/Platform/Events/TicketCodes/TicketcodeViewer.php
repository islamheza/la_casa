<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Platform\Events\TicketCodes;

/**
 * Description of TicketcodeViewer
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;
use App\Models\Ticketcode;
use App\Classes\Core\CURD\BaseViewer;

class TicketcodeViewer extends BaseViewer {

	//put your code here
	protected function createListQuery(Request $request) {
		$query = Ticketcode::query();
		$query->where("event_id", $request->input("event_id"));
		return $query;
	}

}
