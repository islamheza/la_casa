<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Core\Utils;

/**
 * Description of TicketCodeGenerator
 *
 * @author Ahmed Makki
 */
class TicketCodeGenerator {

	//put your code here
	public function newCode($charNumber) {
		$my_string = "123456789abcdefghijklmnpqrstuvwxyz";
		$my_random_string = str_repeat($my_string, 10);
		return substr(str_shuffle($my_random_string), 0, $charNumber);
	}

	public function newPin($charNumber) {
		$my_string = "1234567890";
		$my_random_string = str_repeat($my_string, 5);
		return substr(str_shuffle($my_random_string), 0, $charNumber);
	}

}
