<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Core\Utils;

/**
 * Description of ValidateWebRequestSignature
 *
 * @author Ahmed Makki
 */
class WebRequestSignatureValidator {

    //put your code here
    public function validate($signature, $params, $signKey) {
        $calculatedSign = $this->encrypt($params, $signKey);        
        if ($signature !== $calculatedSign || $signature == null || $signature == "") {
            return false;
        }
        return true;
    }
    
    public function view($params, $signKey){
        $calculatedSign = $this->encrypt($params, $signKey);
        return $calculatedSign;
    }

    public function encrypt($arrData, $signKey) {
        $shaString = '';
        ksort($arrData);
        foreach ($arrData as $k => $v) {
            $shaString .= "$k=$v";
        }
        $shaString = $signKey . $shaString . $signKey;
        $signature = hash("sha512", $shaString);
        return $signature;
    }

}
