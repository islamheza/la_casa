<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Core\Utils;

/**
 * Description of FileUtils
 *
 * @author Ahmed Makki
 */
use Storage;

class FileUtils {

	//put your code here
	public function uploadFile($request, $keyName, $uploadDir, $prepend) {
		if (!$request->file($keyName)) {
			return false;
		}
		$image = $request->file($keyName);
		$fileName = $prepend . time() . rand() . '.' . $image->getClientOriginalExtension();
		$request->file($keyName)->storeAs($uploadDir, $fileName, "public");
		return $fileName;
	}

	public function deleteFileFromLocal($filePath) {
		Storage::disk('public')->delete($filePath);
	}

}
