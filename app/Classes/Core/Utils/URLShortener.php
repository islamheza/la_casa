<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Core\Utils;

/**
 * Description of URLShortener
 *
 * @author Ahmed Makki
 */
use App\Classes\Core\HTTPRequests\HTTPParams;
use App\Classes\Core\HTTPRequests\HTTPSender;
use App\Classes\Core\HTTPRequests\HTTPError;


class URLShortener {

    //put your code here
    private $httpParams;
    private $httpSender;
    private $httpError;
    private $webRequestSignatureValidator;

    const URL = "http://yevents.me/api/createurl";
	const KEY_SHORTENKEY = "6914E1933F630";

    public function __construct(WebRequestSignatureValidator $webRequestSignatureValidator, HTTPSender $httpSender, HTTPParams $httpParams, HTTPError $httpError) {
        $this->webRequestSignatureValidator = $webRequestSignatureValidator;
        $this->httpSender = $httpSender;
        $this->httpParams = $httpParams;
        $this->httpError = $httpError;
    }

    public function getLink($url) {
        $params = array("url" => base64_encode($url));
        $signature = $this->webRequestSignatureValidator->encrypt($params, self::KEY_SHORTENKEY);
        $this->httpParams->reset();
        $this->httpParams->setUrl(URLShortener::URL);
        $this->httpParams->setMethod("POST");
        $this->httpParams->setType(HTTPParams::TYPE_JSON);
        $this->httpParams->addParam("url", $params['url']);
        $this->httpParams->addParam("signature", $signature);
        $response = $this->httpSender->send($this->httpParams);
        $arrayOfResponse = json_decode($response, true);
        
        if (!$arrayOfResponse['state']) {
            return false;
        }
        return $arrayOfResponse["url"];
    }

}
