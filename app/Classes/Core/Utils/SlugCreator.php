<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Core\Utils;

/**
 * Description of WeightCalculator
 *
 * @author AhmedMakki
 */
class SlugCreator {

	//put your code here
	public function create($str) {
		$revert = array(
			'%21' => '!', //!
			'%2A' => '*', //*
			'%27' => "'", //'
			'%28' => '-', //(
			'%29' => '-', //)
			'%3A' => ':', //:
			'%23' => '#', //#
			'%E2%80%93' => '-',
			'%22' => '"', //"
			'%26' => '&', //&
			'%5B' => '[', //[
			'%5D' => ']', //]
			'%E2%80%99' => '’',
			'%2C' => ',',
			'%C3%B6' => 'ö',
			'%C3%A9' => 'é',
			'%2F' => '-', // /
			'%E2%80%9D' => '”', // ”
			'%2B' => '+', // +
		);
		$revertedString = strtr(rawurlencode($str), $revert);
		$explodedArray = explode("%20", $revertedString);
		$stringImploded = implode("-", $explodedArray);
		$replaceOne = str_replace("---", "-", $stringImploded);
		return str_replace("--", "-", $replaceOne);
	}

}
