<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Core\HTTPRequests;

/**
 * Description of HTTPParams
 *
 * @author Ahmed Makki
 */
class HTTPParams {

    //put your code here
    private $url;
    private $params;
    private $getParams;
    private $method;
    private $headers;
    private $type;
    private $verify;

    const TYPE_XFORM = "xfom";
    const TYPE_JSON = "json";

    public function __construct() {
        $this->reset();
    }

    public function addParam($key, $value) {
        $this->params[$key] = $value;
    }

    public function addGETParam($key, $value) {
        $this->getParams[$key] = $value;
    }

    public function addHeader($key, $value) {
        $this->headers[$key] = $value;
    }

    public function getParams() {
        return $this->params;
    }

    public function getUrl() {
        return $this->url;
    }

    public function getMethod() {
        return $this->method;
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function setMethod($method) {
        $this->method = $method;
    }

    public function reset() {
        $this->url = null;
        $this->method = null;
        $this->params = array();
        $this->getParams = array();
        $this->headers = array();
        $this->verify = true;
        $this->type = HTTPParams::TYPE_XFORM;
    }

    public function getHeaders() {
        return $this->headers;
    }

    public function getGetParams() {
        return $this->getParams;
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
    }
    
    public function getVerify() {
        return $this->verify;
    }

    public function setVerify($verify) {
        $this->verify = $verify;
    }



}
