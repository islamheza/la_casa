<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Core\HTTPRequests;

/**
 * Description of HTTPError
 *
 * @author Ahmed Makki
 */
class HTTPError {

    //put your code here
    private $status;
    private $code;
    private $title;
    private $message;
    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

        public function __construct() {
        $this->status = true;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getCode() {
        return $this->code;
    }

    public function getMessage() {
        return $this->message;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function setCode($code) {
        $this->code = $code;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

}
