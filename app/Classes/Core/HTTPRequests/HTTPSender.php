<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Core\HTTPRequests;

/**
 * Description of HTTPSender
 *
 * @author Ahmed Makki
 */
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Message\ResponseInterface;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\stream_for;
use GuzzleHttp\Promise\EachPromise;

class HTTPSender {

    const QUERY_GET = "query";
    const QUERY_POST_TYPE_XFORM = "form_params";
    const QUERY_POST_TYPE_JSON = "json";
    const MAX_CONCURRENCY_CONNECTIONS = 100;

    //put your code here
    private $client;
    private $arrayOfHTTPParams;

    //private $arrayOfAsyncRequests;

    public function __construct() {        
        $this->arrayOfHTTPParams = array();
        //$this->arrayOfAsyncRequests = array();
    }

    public function send(HTTPParams $params) {
        $this->client = new Client();
        $response = $this->client->request($params->getMethod(), $params->getUrl(), $this->getBody($params));
        return $response->getBody()->getContents();
    }

    public function addRequestAsync(HTTPParams $params) {
        //$this->arrayOfAsyncRequests[] = $this->client->requestAsync($params->getMethod(), $params->getUrl(), $this->getBody($params));
//        $body = $this->getBody($params);
//        $this->arrayOfAsyncRequests[] = new Request($params->getMethod(), $params->getUrl(), ["body" => $body]);
        $this->arrayOfHTTPParams[] = $params;
    }

    public function sendAsync() {
        $arrayOfResponses = array();
        $this->client = new Client();
        $client = $this->client;
        $copyThis = $this;
        $params = $this->arrayOfHTTPParams;
        //die("<pre>" . print_r($params, true) . "</pre>");
        $promises = (function () use ($params, $client, $copyThis ) {
                    foreach ($params as $param) {
                        // don't forget using generator
                        yield $client->requestAsync($param->getMethod(), $param->getUrl(), $this->getBody($param));
                    }
                })();
        (new EachPromise($promises, [
    // how many concurrency we are use
    'concurrency' => self::MAX_CONCURRENCY_CONNECTIONS,
    'fulfilled' => function (Response $response) use(&$arrayOfResponses) {
        $arrayOfResponses[] = $response->getBody()->getContents();
    },
    'rejected' => function ($reason) {
        // handle promise rejected here
    }
        ])
        )->promise()->wait();
        $copyThis = null;
        $this->arrayOfHTTPParams = null;
        return $arrayOfResponses;

        //Divide The Requests by yourself
//        $arrayOfGroups = array_chunk($this->arrayOfAsyncRequests, self::MAX_CONCURRENCY_CONNECTIONS);
//        $arrayOfResponses = array();
//        foreach ($arrayOfGroups as $group):
//            $results = \GuzzleHttp\Promise\settle($group)->wait();
//            foreach ($results as $result):
//                $response = $result['value'];
//                $arrayOfResponses[] = $response->getBody()->getContents();
//            endforeach;
//        endforeach;
//        die("<pre>" . print_r($arrayOfResponses, true) . "</pre>");
//        return $arrayOfResponses;
    }

    public function sendCopy(HTTPParams $params) {
        $this->client = new Client();
        $queryKeyword = $params->getMethod() == "GET" ? HTTPSender::QUERY_GET : HTTPSender::QUERY_POST;
        $response = $this->client->request($params->getMethod(), $params->getUrl(), [$queryKeyword => $params->getParams()]);
        return $response->getBody()->getContents();
    }

    private function getBody(HTTPParams $params) {
        $body = array();
        $body[HTTPSender::QUERY_GET] = $params->getGetParams();
        if ($params->getMethod() == "GET") {
            $mergedArray = array_merge($params->getGetParams(), $params->getParams());
            $body[HTTPSender::QUERY_GET] = $mergedArray;
        } elseif ($params->getMethod() == "POST") {
            switch ($params->getType()) {
                case HTTPParams::TYPE_JSON:
                    $body[HTTPSender::QUERY_POST_TYPE_JSON] = $params->getParams();
                    break;
                case HTTPParams::TYPE_XFORM:
                    $body[HTTPSender::QUERY_POST_TYPE_XFORM] = $params->getParams();
                    break;
            }
        }
        $body[RequestOptions::VERIFY] = $params->getVerify();
        //$body[RequestOptions::VERIFY] = false;
        return $body;
    }

}
