<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Core\CURD;

/**
 * Description of BaseCreator
 *
 * @author AhmedMakki
 */
use App\Classes\Core\CURD\Contracts\Populator;
use Illuminate\Http\Request;

abstract class BaseCreator {

	const STATUS_SUCCESSVALIDATION = 0;
	const STATUS_VALDATIONFAILED = -1;
	const STATUS_BEFORESAVEFAILED = -2;

	//put your code here
	protected $record;
	protected $populators;

	function __construct() {
		$this->populators = array();
	}

	public function addPopulator(Populator $populator) {
		$this->populators[] = $populator;
	}

	abstract function initRecord(Request $request);

	abstract function beforeSaveCheck(Request $request);

	abstract function afterSave(Request $request);

	public function process(Request $request) {
		$this->initRecord($request);
		list($validateStatus, $validationError) = $this->processBasicValidation($request);
		if (!$validateStatus) {
			return array(self::STATUS_VALDATIONFAILED, $validationError);
		}
		$this->processPopulators($request);
		list($beforeProcessCheckStatus, $beforeProcessError) = $this->beforeSaveCheck($request);
		if ($beforeProcessCheckStatus === false) {
			return array(self::STATUS_BEFORESAVEFAILED, $beforeProcessError);
		}
		$this->record->save();
		$this->afterSave($request);
		return array(self::STATUS_SUCCESSVALIDATION, $this->record);
	}

	private function processBasicValidation(Request $request) {
		$validateStatus = true;
		foreach ($this->populators as $populator):
			if ($populator->validate($request)->fails()) {
				$validateStatus = false;
				break;
			}
		endforeach;
		return array($validateStatus, $populator->validate($request)->messages());
	}

	private function processPopulators(Request $request) {
		foreach ($this->populators as $populator):
			$populator->set($this->record, $request);
		endforeach;
	}

}
