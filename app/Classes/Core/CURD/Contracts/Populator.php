<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Core\CURD\Contracts;

/**
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;

interface Populator {

	//put your code here
	public function set($record, Request $request);

	public function validate(Request $request);
}
