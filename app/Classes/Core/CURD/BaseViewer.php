<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes\Core\CURD;

/**
 * Description of BaseViewer
 *
 * @author AhmedMakki
 */
use Illuminate\Http\Request;

abstract class BaseViewer {

	const NUMBEROFROWS = 25;

	//put your code here
	public function get(Request $request) {
		$pageNumber = $request->input("pagenumber", 0);
		$offset = (int) $pageNumber * self::NUMBEROFROWS;
		$list = $this->createListQuery($request)->offset($offset)->limit(self::NUMBEROFROWS)->get();
		$total = $this->createListQuery($request)->count();
		return array($list, $total);
	}

	abstract protected function createListQuery(Request $request);
}
