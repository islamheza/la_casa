<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model {

	use HasFactory;

	public function sections() {
		return $this->hasMany('App\Models\Section');
	}

	public function tickettypes() {
		return $this->hasMany('App\Models\Tickettype');
	}

	public function speakers() {
		return $this->hasMany('App\Models\Speaker');
	}

	public function sponsors() {
		return $this->hasMany('App\Models\Speaker');
	}

	public function booths() {
		return $this->hasMany('App\Models\Booth');
	}



}
