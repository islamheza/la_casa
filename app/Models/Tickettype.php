<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tickettype extends Model {

	use HasFactory;

	public function ticketcodes() {
		return $this->belongsToMany('App\Models\Ticketcode', 'ticketcodetickettypes');
	}

}
