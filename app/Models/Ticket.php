<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model {

	use HasFactory;

	public function qrcode() {
		return $this->belongsTo('App\Models\Qrcode');
	}

	public function type() {
		return $this->belongsTo('App\Models\Tickettype', "tickettype_id");
	}

}
