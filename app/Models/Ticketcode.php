<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticketcode extends Model {

	use HasFactory;

	public function tickettypes() {
		return $this->belongsToMany('App\Models\Tickettype', 'ticketcodetickettypes');
	}

}
