<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booth extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'about' , 'address' ,  'logo' , 'image' ,
         'video' , 'website' , 'facebook' ,'instagram' , 
         'youtube' , 'event_id'
    ];

    public function teamMembers() {
		return $this->hasMany('App\Models\TeamMember');
	}
}
